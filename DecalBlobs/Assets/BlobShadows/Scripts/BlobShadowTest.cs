﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobShadowTest : MonoBehaviour
{
    [SerializeField]
    private Light _light;
    [SerializeField]
    private GameObject _shadowProjector;
    [SerializeField]
    private GameObject _shadowDecal;

    private void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 200, 50), "Enable Realtime Shadows"))
        {
            _light.shadows = LightShadows.Soft;
            _shadowDecal.SetActive(false);
            _shadowProjector.SetActive(false);
        }
        if (GUI.Button(new Rect(0, 50, 200, 50), "Enable Shadow Projector"))
        {
            _light.shadows = LightShadows.None;
            _shadowDecal.SetActive(false);
            _shadowProjector.SetActive(true);
        }
        if (GUI.Button(new Rect(0, 100, 200, 50), "Enable Shadow Decal"))
        {
            _light.shadows = LightShadows.None;
            _shadowDecal.SetActive(true);
            _shadowProjector.SetActive(false);
        }
    }
}
